require essioc
require adsis8300bcm

# set macros
epicsEnvSet("CONTROL_GROUP", "PBI-BCM01")
epicsEnvSet("AMC_NAME",      "Ctrl-AMC-210")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",      "PBI-BCM01:Ctrl-EVR-201:")

epicsEnvSet("P",        "$(CONTROL_GROUP):")
epicsEnvSet("R",        "$(AMC_NAME):")
epicsEnvSet("PREFIX",   "$(P)$(R)")

# set BCM generic channel names
iocshLoad("bcm-channels.iocsh")

# set BCM specific channel names
epicsEnvSet("SYSTEM1_PREFIX",  "ISrc-010:PBI-BCM-201:")
epicsEnvSet("SYSTEM1_NAME",    "PBI-BCM01-R#1 - ISrc-010")
epicsEnvSet("SYSTEM1_ARCHIVER","")

epicsEnvSet("SYSTEM2_PREFIX",  "LEBT-010:PBI-BCM-201:")
epicsEnvSet("SYSTEM2_NAME",    "PBI-BCM01-R#2 - LEBT-010")
epicsEnvSet("SYSTEM2_ARCHIVER","")

epicsEnvSet("SYSTEM3_PREFIX",  "RFQ-010:PBI-BCM-201:")
epicsEnvSet("SYSTEM3_NAME",    "PBI-BCM01-R#3 - RFQ-010")
epicsEnvSet("SYSTEM3_ARCHIVER","")

epicsEnvSet("SYSTEM4_PREFIX",  "MEBT-010:PBI-BCM-201:")
epicsEnvSet("SYSTEM4_NAME",    "PBI-BCM01-R#4 - MEBT-010")
epicsEnvSet("SYSTEM4_ARCHIVER","")

epicsEnvSet("SYSTEM5_PREFIX",  "MEBT-010:PBI-BCM-202:")
epicsEnvSet("SYSTEM5_NAME",    "PBI-BCM01-R#5 - MEBT-010")
epicsEnvSet("SYSTEM5_ARCHIVER","")

epicsEnvSet("SYSTEM6_PREFIX",  "DTL-010:PBI-BCM-201:")
epicsEnvSet("SYSTEM6_NAME",    "PBI-BCM01-R#6 - DTL-010")
epicsEnvSet("SYSTEM6_ARCHIVER","")

epicsEnvSet("SYSTEM7_PREFIX",  "DTL-020:PBI-BCM-201:")
epicsEnvSet("SYSTEM7_NAME",    "PBI-BCM01-R#7 - DTL-020")
epicsEnvSet("SYSTEM7_ARCHIVER","")

epicsEnvSet("SYSTEM8_PREFIX",  "ISrc-010:ISS-HVPf-201:")
epicsEnvSet("SYSTEM8_NAME",    "PBI-BCM01-R#8 - ISrc-010 HVPf")
epicsEnvSet("SYSTEM8_ARCHIVER","")



# load BCM data acquistion
iocshLoad("$(adsis8300bcm_DIR)/bcm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

# custom autosave in order to have Lut ID PVs restored before the others
afterInit("makeAutosaveFileFromDbInfo('$(AS_TOP)/$(IOCDIR)/req/lutIDs.req','autosaveFieldsLutIDs')")
afterInit("create_monitor_set("lutIDs.req",5)")
afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/lutIDs.sav")")
afterInit("epicsThreadSleep(2)")

afterInit("fdbrestore("$(AS_TOP)/$(IOCDIR)/save/settings.sav")")

# call iocInit
iocInit

date

